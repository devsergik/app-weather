package com.test.app

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.test.app.adapter.CitiesAdapter
import com.test.weather.WeatherClass
import com.test.weather.data.model.CityModel
import com.test.weather.listener.WeatherListener
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity(), WeatherListener {

    var weatherClass: WeatherClass? = null
    var cities :MutableList<CityModel> = mutableListOf()

    override fun onSearchResultRequest(items: List<CityModel>) {
        cities.clear()
        cities.addAll(items)
        adapter.notifyDataSetChanged()
    }

    override fun onError(errorRes: Int) {
        Toast.makeText(applicationContext,
            getString(errorRes), Toast.LENGTH_SHORT).show()
    }

    private val adapter by lazy {
        CitiesAdapter(
            cities = cities,
            clickItem = { position -> weatherClass?.getCurrentWeather(cities[position].woeid)}
        )
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        rv_cities.let {
            it.layoutManager = LinearLayoutManager(this)
            it.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
            it.adapter = this.adapter
        }

        weatherClass = WeatherClass()
        weatherClass!!.setWeatherListener(this)

        btn_send.setOnClickListener { weatherClass!!.searchCity(et_query.text.toString()) }
    }

    override fun onSuccessWeather(items: Array<String>) {
        showAlertDialog(items)
    }


    fun showAlertDialog(items: Array<String>){
        val builder = AlertDialog.Builder(this)
        builder.setTitle(getString(R.string.weather_title_alert))
        builder.setItems(items) { dialog, which ->
        }
        builder.setPositiveButton(android.R.string.yes) { dialog, which ->
        }
        builder.show()
    }

}
