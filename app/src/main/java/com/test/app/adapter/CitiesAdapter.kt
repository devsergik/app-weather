package com.test.app.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.test.app.R
import com.test.weather.data.model.CityModel

class CitiesAdapter(
                    private val cities: MutableList<CityModel>,
                    private val clickItem: (Int) -> Unit)
    : RecyclerView.Adapter<CitiesViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CitiesViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return CitiesViewHolder(inflater, parent)
    }

    override fun onBindViewHolder(holder: CitiesViewHolder, position: Int) {
        val movie: CityModel = cities[position]
        holder.bind(position, movie, holder.itemView, clickItem)
    }

    override fun getItemCount(): Int = cities.size


}

class CitiesViewHolder(inflater: LayoutInflater, parent: ViewGroup) :
    RecyclerView.ViewHolder(inflater.inflate(R.layout.list_item_city, parent, false)) {
    private var mTitleCity: TextView? = null

    init {
        mTitleCity = itemView.findViewById(R.id.tv_list_title)
    }

    fun bind(position: Int, movie: CityModel, itemView: View, clickItem: (Int) -> Unit) {
        mTitleCity?.text = movie.title
        itemView.setOnClickListener { clickItem(position) }
    }

}