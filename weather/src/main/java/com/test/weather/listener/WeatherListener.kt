package com.test.weather.listener

import com.test.weather.data.model.CityModel
import com.test.weather.data.model.ConsolidateWeather

interface WeatherListener {

    /*
    * Результат поиска города
    * */
    fun onSearchResultRequest(items: List<CityModel>)

    /*
    * Успешное запрос погоды
    * */
    fun onSuccessWeather(items: Array<String>)

    /*
    * Ошибка выполнения
    * */
    fun onError(errorRes: Int)
}