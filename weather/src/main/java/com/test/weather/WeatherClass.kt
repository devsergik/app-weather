package com.test.weather

import com.test.weather.data.api.RestApi
import com.test.weather.listener.WeatherListener
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class WeatherClass  {

    private var weatherListener: WeatherListener? = null

    fun setWeatherListener(weatherListener: WeatherListener){
        this.weatherListener = weatherListener
    }

    /*
    * Метод получения погоды по @param id города
    * @param id
    * */
    fun getCurrentWeather(id: Int){
        RestApi.getWeather(id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if(it.consolidated_weather?.isNotEmpty()!!){
                    weatherListener?.onSuccessWeather(
                        it.consolidated_weather?.map{it.applicable_date.plus(" (").plus(it.weather_state_name).plus(")") }!!
                            .toTypedArray())
                } else {
                    weatherListener?.onError( R.string.error_empty_list)
                }
            }, {
                weatherListener?.onError( R.string.error_server_connect)
            })
    }

    /*
    * Метод поиска города
    * @param location
    * */
    fun searchCity(location: String){
        RestApi.getCity(location)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if(it.isNotEmpty()){
                    weatherListener?.onSearchResultRequest(it)
                } else {
                    weatherListener?.onError( R.string.error_empty_list)
                }
            }, {
                weatherListener?.onError( R.string.error_server_connect)
            })
    }

}