package com.test.weather.data.model

data class CityModel(
    var title: String,
    var location_type: String,
    var woeid: Int,
    var latt_long: String
)