package com.test.weather.data.api

import com.test.weather.data.model.CityModel
import com.test.weather.data.model.WeatherModel
import io.reactivex.Single
import retrofit2.http.*


interface API {

    @GET("api/location/search")
    fun getCity(@Query("query") query: String): Single<List<CityModel>>

    @GET("api/location/{id}")
    fun getWeather(@Path("id") id: Int): Single<WeatherModel>
}