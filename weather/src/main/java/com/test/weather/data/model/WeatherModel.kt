package com.test.weather.data.model

data class WeatherModel (
    var consolidated_weather: List<ConsolidateWeather>? = listOf()
)

class ConsolidateWeather(
    var id: Long,
    var weather_state_name: String,
    var applicable_date: String
)